use std::env;
extern crate base32;
extern crate oath;
use oath::{totp_raw_now, HashType};

fn main() {
    let args: Vec<String> = env::args().collect();
    let key_name = env::var(&args[1]);
    if key_name.is_err() {
      panic!("Requested TOTP key not found in environment variables.");
    }

    let key_data = key_name.unwrap().to_uppercase();
    let base32_data = base32::decode(base32::Alphabet::RFC4648 {padding:false}, &key_data);
    let base32_value = base32_data.unwrap();
    let token = totp_raw_now(&base32_value, 6, 0, 30, &HashType::SHA1);
    println!("{}", format!("{:06}", token));
}
