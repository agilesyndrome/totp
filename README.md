# totp for Go

## TL;DR... but you should really read the rest

Making two factor authentication codes, easy & automatable

1. Setup an ENV var with your two factor authentication key. (Pro Tip.. you can usually get this by clicking the "can't scan barcode" link below the QR code that is presented.)

1. Add this key to your environment variable, i.e. APP_MFAKEY

1. Run `totp APP_MFAKEY`  One a mac? Try `totp APP_MFAKEY |pbcopy` and the key will be copied to your clipboard.

## Securing environment variables

* Don't put these keys in .bash_profile or .bashrc!  This is unsecure. This app is best used when paired with an app like `envchain` that can safely store secrets in Keychain, and expose them as environment variables.  Also available is `summon` for AWS secrets. 

## Building

1. `cargo build --release`
